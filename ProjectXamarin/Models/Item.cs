﻿using System;

namespace ProjectXamarin.Models
{
    public class Item
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Faculty { get; set; }
        public int Year { get; set; }
        public int Age { get; set; }
        public string Phone { get; set; }
        public string ImageLink { get; set; }
    }
}