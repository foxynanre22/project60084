﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using ProjectXamarin.Services;
using System.Threading.Tasks;

namespace ProjectXamarin.iOS
{
    class AppleTestA : ITestA
    {
        public async Task<string> GetPlatform()
        {
            return "Apple";
        }
    }
}