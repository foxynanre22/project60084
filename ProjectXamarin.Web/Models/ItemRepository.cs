﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace ProjectXamarin.Models
{
    public class ItemRepository : IItemRepository
    {
        private static ConcurrentDictionary<string, Item> items =
            new ConcurrentDictionary<string, Item>();

        public ItemRepository()
        {
            if (items.Count==0)
            {
                Add(new Item { Id = Guid.NewGuid().ToString(), Username = "DanI9", Password = "123", Name = "Denial", Surname = "Martin", Faculty = "IT", Year = 1, Age = 19, Phone = "881996826", ImageLink = "cover.jpg" });
                Add(new Item { Id = Guid.NewGuid().ToString(), Username = "Mish0", Password = "123", Name = "Michał", Surname = "Kowalski", Faculty = "IT", Year = 1, Age = 21, Phone = "177956299", ImageLink = "" });
                Add(new Item { Id = Guid.NewGuid().ToString(), Username = "CiPe2", Password = "123", Name = "Cizare", Surname = "Petruchi", Faculty = "IT", Year = 2, Age = 20, Phone = "15698003", ImageLink = "" });
            }
           
        }

        public IEnumerable<Item> GetAll()
        {
            return items.Values;
        }

        public void Add(Item item)
        {
            item.Id = Guid.NewGuid().ToString();
            items[item.Id] = item;
        }

        public Item Get(string id)
        {
            items.TryGetValue(id, out Item item);
            return item;
        }

        public Item Remove(string id)
        {
            items.TryRemove(id, out Item item);
            return item;
        }

        public void Update(Item item)
        {
            items[item.Id] = item;
        }
    }
}
