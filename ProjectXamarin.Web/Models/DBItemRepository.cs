﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectXamarin.Models;
using ProjectXamarin.Web.Data;

namespace ProjectXamarin.Web.Models
{
    public class DBItemRepository : IItemRepository
    {
        private readonly ProjectDBContext _DBContext;
        public DBItemRepository(ProjectDBContext myDbContext)
        {
            _DBContext = myDbContext;
        }


        public void Add(Item item)
        {
            _DBContext.Items.Add(item);
            _DBContext.SaveChanges();
        }

        public Item Get(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Item> GetAll()
        {
            return _DBContext.Items.ToList();
        }

        public Item Remove(string key)
        {
            throw new NotImplementedException();
        }

        public void Update(Item item)
        {
            _DBContext.Items.Add(item);
            _DBContext.SaveChanges();
        }
    }
}
