﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectXamarin.Models;
using Microsoft.EntityFrameworkCore;

namespace ProjectXamarin.Web.Data
{
    public class ProjectDBContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\MSSQLSERPROJECTS; Database=w60084; Trusted_Connection=True;");
        }


        public DbSet<Item> Items { get; set; }
    }
}
